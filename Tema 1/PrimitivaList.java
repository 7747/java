import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
public class PrimitivaList {
	public static int generarAleatorio(int inf, int sup) {
		//[0,1) ---> [inf,sup+1)
		//a*0+b=inf  ==> b=inf
		//a*1+b=sup  ==> a=sup-b=sup-inf
		int a=sup-inf, b=inf;
		return (int) (a*Math.random()+b);
	}
	public static void prueba() {
		int i =0;
		bucleext:
//		System.out.println("hola");
		while (i<100) {
		    i++ ;
		    for(int j=0;j<i;j++) {
		        System.out.print("*");
		        if ( i == 5 ) {break bucleext ;}
		    }
		    System.out.println("");
		}
	}
	public static void main(String args[]) {
		//List<Obj> list = new ArrayList<Obj> ();
		List<Integer> lista=new ArrayList<Integer>();
		int na; // número aleatorio
		while (lista.size()<6) {
			na=generarAleatorio(1,49);
			if (!lista.contains(na)) {
				lista.add(na);
			}
		}
		Collections.sort(lista);
		System.out.println(lista);
	}
}