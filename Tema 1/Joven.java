public class Joven {
	String nombre;
	String apellido1;
	String apellido2;
	int edad;
	int nivelDeEstudios;
	double ingresos;
	boolean jasp;

	public Joven(String nombre, String apellido1, String apellido2,int edad, int nivelDeEstudios, double ingresos) {
		//short edad;
		//byte nivelDeEstudios;
		//double ingresos;
		this.nombre=nombre;
		this.apellido1=apellido1;
		this.apellido2=apellido2;
		this.edad=edad;
		this.nivelDeEstudios=nivelDeEstudios;
		this.ingresos=ingresos;
	}

	public void establecerJasp() {
		this.jasp=(this.edad<=28 && this.nivelDeEstudios>3 && this.ingresos>28000);
	}
	public static void main(String args[]) {
		Joven p=new Joven("Pepito","López","Pérez",18,3,28000.65);
		Joven q=new Joven("Luisa","López","Martínez",15,2,0.0);
		p.establecerJasp();
		q.establecerJasp();
		System.out.println(p.jasp);

	}

}