public class ecuacion2o {

	double a,b,c;
	public double discriminante(){
		return b*b-4*a*c;
	}
	public double solucion1(){
		if (discriminante()>=0) {
			return (-b+Math.sqrt(discriminante())/(2*a));
		}
		return Double.NaN;
	}
	public double solucion2(){
		if (discriminante()>=0) {
			return (-b-Math.sqrt(discriminante())/(2*a));
		}
		return Double.NaN;
	}
}
