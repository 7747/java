public class ecuacion2 {
	public static void main(String[] args) {
		double a,b,c,disc,x1,x2;
		a=Double.parseDouble(args[0]);
		b=Double.parseDouble(args[1]);
		c=Double.parseDouble(args[2]);
		disc=b*b-4*a*c;
		if (disc>0) {
			x1=(-b+Math.sqrt(disc))/(2*a);
			x2=(-b-Math.sqrt(disc))/(2*a);
			System.out.println("Hay dos soluciones"+x1+","+x2);
		} else if (disc==0) {
			x1=x2=-b/(2*a);
			System.out.println("Hay una solucion unica:"+x1);
		} else {
			System.out.println("No hay soluciones reales");
		}
	}
}