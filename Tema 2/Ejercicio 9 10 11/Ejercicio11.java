public class Ejercicio11 {
    public static void main(String[] args){
        int contador,p,numero;
        numero=Integer.parseInt(args[0]);
        contador = 0;
        for(p=1;p<=numero;p++)
        {
            if(numero%p==0)
            {
                contador++;
            }
        }
 
        if(contador<=2)
        {
            System.out.println("El numero "+numero+" es primo");
        }else{
            System.out.println("El numero "+numero+" no es primo");
        }
    }
}