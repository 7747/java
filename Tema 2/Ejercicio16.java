public class Ejercicio16 {
	
	public static String repite(String s, int n){
		String res="";
		for (int i=1;i<=n;i++) {
			res=res+s;
		}
		return res;
	}



	public static void main(String[] args) {
		int nEsp, nAst=1, n;
		n=Integer.parseInt(args[0]);//dando por hecho que seea un numero positivo e impar

		nEsp=(n-1)/2;//division exacta ya que numore impar -1=par par/par exacto


		//para piramide for(int fila=1;fila<=(n+1)/2;fila++)
		for(int fila=1;fila<=n;fila++){

			System.out.println(repite("  ",nEsp)+repite("* ",nAst));
			if (fila<(n+1)/2) {
				nEsp--;
				nAst+=2;
			}else {
				nEsp++;
				nAst-=2;
			}	
		}

	
	}
}

