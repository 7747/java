public class Ejercicio8 {
	public static void main(String[] args) {
		int n1,n2;

		n1=Integer.parseInt(args[0]);
		n2=Integer.parseInt(args[1]);
		System.out.println("La potencia de "+n1+" elevado a "+n2+" = "+Math.pow(n1,n2));
	}

}